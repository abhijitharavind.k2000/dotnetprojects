﻿// See https://aka.ms/new-console-template for more information
using Joints.Model;

List<Contact> contacts;
contacts = new List<Contact>()
{
    new Contact(){Id=1, Name="Abhijith"},
    new Contact(){Id=2, Name="Abhi"}
};
List<Product> products;
products = new List<Product>()
{
    new Product(){ProductId=1, ProductName="Mobile"},
    new Product(){ProductId=2, ProductName="Phone"}
};
Console.WriteLine("Joint ");
var result = from a in products
             join b in contacts on a.ProductId equals b.Id
             select new
             {
                 PName=a.ProductName,
                 CName=b.Name
             };
foreach (var contact in result)
{
    Console.WriteLine($"{contact.PName}\t{contact.CName}");
}