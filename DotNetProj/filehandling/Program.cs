﻿// See https://aka.ms/new-console-template for more information


using filehandling.Exceptions;
using filehandling.Model;
using filehandling.repository;

UserRepository userRepository = new UserRepository();
IUserRepository iuserRepository = (IUserRepository) userRepository;
//bool isUserRegistered = iuserRepository.RegisterUser(new User() { Id = 1, Name = "user1", City = "Delhi" });




try
{
    Console.Write("Enter the Id no: ");
    int id= Convert.ToInt32(Console.ReadLine());
    Console.Write("Enter the Name no: ");
    string name = Console.ReadLine();
    Console.Write("Enter the City no: ");
    string city = Console.ReadLine();


    bool isUserRegistered = iuserRepository.RegisterUser(new User() { Id=id , Name=name , City=city});
    if (isUserRegistered)
    {
        Console.WriteLine("Done Saving");
    }
}
   

catch (InvalidUserException ex)

{
    Console.WriteLine(ex.Message);
}

List<string> userContent = userRepository.ReadContentsFromFile("userDatabase.txt");
foreach (string fledata in userContent)
{
    Console.WriteLine(fledata);
}