﻿using filehandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.repository
{
    internal interface IFiles
    {
        void WriteContentsToFile(User user, string filename);
        void ReadContentsFromFile(string filename);
        bool IsUsernameExists(string userName, string filename);
    }
}
