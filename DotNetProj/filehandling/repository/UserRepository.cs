﻿using filehandling.Exceptions;
using filehandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.repository
{
    internal class UserRepository : IUserRepository
    {
        List<User> users;

        public UserRepository()
        {
            users = new List<User>();
        }
        
        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            bool isUserNameExist = IsUsernameExists(user.Name, fileName);
            if (isUserNameExist)
            {
                return false;
            }
            else
            {
                WriteContentsToFile(user, fileName);
                return true;
            }

            }
        public bool IsUsernameExists(string userName, string filename)
        {
            
            bool isUserAvailable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowline;
                
                while ((rowline = sr.ReadLine()) != null)
                {
                    string[] rowSplittedValues = rowline.Split(",");
                    
                    foreach (string userIndividualvalue in rowSplittedValues)
                    {
                        if (userIndividualvalue == userName)
                        {
                            throw new InvalidUserException("User already exist");
                            //isUserAvailable = true;
                            //break;
                        }
                    }
                }
                return isUserAvailable;

            }
        }


        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using(StreamReader sr=new StreamReader(fileName))
            {
                string rowLine;
                while((rowLine = sr.ReadLine())!= null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;            
            }
            }


            private void WriteContentsToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))
            {
                sw.Write($"{user}");
            }
        }
    }
}