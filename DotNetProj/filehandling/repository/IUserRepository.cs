﻿using filehandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filehandling.repository
{
    internal interface IUserRepository
    {
        bool RegisterUser(User user);
    }
}
